import java.time.LocalDate;
import java.util.Objects;

public class Car implements Comparable<Car>{
	private String brand;
	private double power;
	private LocalDate introductionDate;

	public Car(String brand, double power, LocalDate introductionDate)  {
		this.brand = brand;
		this.power = power;
		this.introductionDate = introductionDate;
	}

	public Car(String brand, double power) {
		this.brand = brand;
		this.power = power;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public double getPower() {
		return power;
	}

	public void setPower(double power) {
		this.power = power;
	}

	@Override
	public String toString() {
		return "Car{" +
			"brand='" + brand + '\'' +
			", power=" + power +
			",introduction date " + introductionDate+
			'}';
	}

	@Override
	public int compareTo(Car o) {
		//return (int)( power - o.power);
		return  power > o.power?1:-1;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Car car = (Car) o;
		return Double.compare(power, car.power) == 0 &&
			Objects.equals(brand, car.brand);
	}

	@Override
	public int hashCode() {
		return Objects.hash(brand, power);
	}

	//@Override
	// sorting on date
//	public int compareTo(Car o) {
//		return introductionDate.compareTo(o.introductionDate);
//	}

}
