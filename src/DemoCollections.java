import java.util.*;

public class DemoCollections {
	public static void main(String[] args) {
		Car[] cars = new Car[2];
		cars[0] = new Car("Volkswagen", 70);
		cars[1] = new Car("Renault", 100);
		// cannot put a plane in a cars array
		//cars[0] = new Plane("Airbus", 800);

		// printing of cars array, just shows the hascode
		//System.out.println("cars: " + cars);

		// printing with Arrays
		System.out.println("cars: " + Arrays.toString(cars));

		// Collections
		List carsList = new ArrayList();
		carsList.add(new Car("tesla", 300));
		carsList.add(new Car("mini", 60));
		System.out.println("List of cars: " + carsList);
		carsList.add(new Plane("Boeing", 900));
		System.out.println("List of cars: " + carsList);
		System.out.println("first car: " + carsList.get(1));

		// cannot add beyond the size: indexOutOfBoundException
		// carsList.add(1000, new Car("gokart", 20));
		// System.out.println("List of cars: " + carsList);

		int[] ages = {1, 5, 9};

		carsList.add(Integer.valueOf(5));
		carsList.add(6);

		System.out.println("with numbers: " + carsList);

		//List<Car> onlyCars = new ArrayList<>();
		// Cannot put planes in List<Car>
		//onlyCars.add(new Plane("F35",1500));

		// list of: a readonly list
		// make it mutable by passing it to a constructor (if needed)
		List<Car> onlyCars = new LinkedList<>(List.of(
			new Car("tesla", 300),
			new Car("citroen", 120),
			new Car("mini", 60)
		));
		onlyCars.add(new Car("mercedes",230));
		for (Car car : onlyCars) {
			// this can give problems
			// onlyCars.remove(car);
			// use an iterator!
			System.out.println(car);
		}

		for(Iterator <Car> it = onlyCars.iterator();it.hasNext();){
			Car car = it.next();
			System.out.println("next car: " +car);
			if(car.getPower() > 100){
				it.remove();
			}
		}
		System.out.println("After removal: " + onlyCars);


	}
}