import java.time.LocalDate;
import java.util.*;

public class Demo3Set {
	public static void main(String[] args) {
		Set<String> students = new HashSet<>(
			Set.of("Brent", "Shafiga", "Reno", "Gagan", "Ann")
		);
		students.add("Hyuliya");
		System.out.println(students);
		students.add("Reno");
		System.out.println(students);

		Set<Car> cars = new HashSet<Car>(Set.of(
			new Car("tesla", 300, LocalDate.of(2012, 10, 6)),
			new Car("citroen", 120, LocalDate.of(2020, 6, 12)),
			new Car("mini", 60, LocalDate.of(2015, 8, 18))
		));
		cars.add(new Car("tesla", 300, LocalDate.of(2022, 10, 6))
		);
		System.out.println(cars);

	}
}
