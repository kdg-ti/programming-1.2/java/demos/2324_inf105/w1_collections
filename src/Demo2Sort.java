import java.time.LocalDate;
import java.util.*;

public class Demo2Sort {

	public static void main(String[] args) {
		// put new ArrayList around this because list.of returns an immutable collection
		// that cannot be changed for sorting
		List <String> students = new ArrayList<>(
			List.of("Brent","Shafiga","Reno","Gagan","Ann")
			 )
			;
		System.out.println(students);
		Collections.sort(students);
		System.out.println(students);
		List <Car> cars =new ArrayList<Car>(List.of(
			new Car("tesla", 300, LocalDate.of(2012,10,6)),
			new Car("citroen", 120,LocalDate.of(2020,6,12)),
			new Car("mini", 60,LocalDate.of(2015,8,18))
		));
		System.out.println(cars);
	   Collections.sort(cars);
		System.out.println("sorted cars: "+cars);
	}
}
