public class Plane {
	private String brand;
	private int maxSpeed;

	public Plane(String brand, int maxSpeed) {
		this.brand = brand;
		this.maxSpeed = maxSpeed;
	}

	@Override
	public String toString() {
		return "Plane{" +
			"brand='" + brand + '\'' +
			", maxSpeed=" + maxSpeed +
			'}';
	}
}
